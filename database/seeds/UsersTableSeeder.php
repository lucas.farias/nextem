<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Teste',
            'email' => 'teste@nextem.com.br',
            'password' => bcrypt('1234'),
            'is_admin' => 1,
        ]);

        User::create([
            'name' => 'Teste2',
            'email' => 'teste2@gmail.com.br',
            'password' => bcrypt('1234'),
            'is_admin' => 0,
        ]);

        User::create([
            'name' => 'Teste3',
            'email' => 'teste3@gmail.com.br',
            'password' => bcrypt('1234'),
            'is_admin' => 0,
        ]);

        User::create([
            'name' => 'Teste4',
            'email' => 'teste4@gmail.com.br',
            'password' => bcrypt('1234'),
            'is_admin' => 0,
        ]);
    }
}
