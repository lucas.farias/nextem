<?php

use App\Status;
use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::create([
            'name' => 'To Do',
        ]);

        Status::create([
            'name' => 'WIP',
        ]);

        Status::create([
            'name' => 'Review',
        ]);

        Status::create([
            'name' => 'Done',
        ]);
    }
}
