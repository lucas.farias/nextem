<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap Simple Login Form</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .login-form {
            width: 340px;
            margin: 50px auto;
        }
        .login-form form {
            margin-bottom: 15px;
            background: #f7f7f7;
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            padding: 30px;
        }
        .login-form h2 {
            margin: 0 0 15px;
        }
        .form-control, .btn {
            min-height: 38px;
            border-radius: 2px;
        }
        .btn {
            font-size: 15px;
            font-weight: bold;
        }
    </style>
    <title>Home Page</title>
</head>
<body>
    <div class="flex-center position-ref full-height">
        <h2>
            Add Activity
        </h2>
        <div>
            <table class="table table-striped">
            <form method="POST" action="{{ route('admin.storeActivity', ['id' => $id]) }}">
            @csrf
            <thead>
                <tr>
                    <th scope="col">Name of the activity</th>
                    <th scope="col">Description of the activity</th>
                    <th scope="col">Responsible</th>
                    <th scope="col">Status</th>
                    <th scope="col">Deadline</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="text" name="name"/></td>
                    <td><input type="text" name="description"/></td>
                    <td>
                        <select name="users_id">
                            <option>Select</option>
                            @foreach ($users as $user)
                                @if($user->is_admin == 0)
                                    <option value="{{ $user->id }}">{{ $user->email }}</option>
                                @endif
                            @endforeach
                        </select>
                    </td>
                    <td>
                        <select name="status_id">
                            <option>Select</option>
                            @foreach ($status as $statu)
                                <option value="{{ $statu['id'] }}">
                                    {{ $statu['name'] }}
                                </option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        <input type="date" name="date_end"/>
                    </td>
                    <td>
                        <input type="submit" value="Send"/>
                    </td>
                </tr>
            </tbody>
            </table>
        </div>
        <div>

        </div>
        <br>
        <a href="{{ route('logout') }}">Logout</a>
    </div>
</body>
</html>
