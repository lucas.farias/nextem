<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap Simple Login Form</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .login-form {
            width: 340px;
            margin: 50px auto;
        }
        .login-form form {
            margin-bottom: 15px;
            background: #f7f7f7;
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            padding: 30px;
        }
        .login-form h2 {
            margin: 0 0 15px;
        }
        .form-control, .btn {
            min-height: 38px;
            border-radius: 2px;
        }
        .btn {
            font-size: 15px;
            font-weight: bold;
        }
    </style>
    <title>Home Page</title>
</head>
<body>
    <div class="flex-center position-ref full-height">

        <div class="form-row align-items-center">
            <form method="POST" action="{{ route('admin.store') }}">
            @csrf
                <div class="col-auto my-1">
                    <h2>
                        Create project
                    </h2>
                </div>
                <div class="col-sm-2 my-1">

                    <input type="text" class="form-control mb-2" name="name" placeholder="Name project"/>

                </div>
                <div class="col-sm-2 my-1">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>

        <div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Project</th>
                        <th scope="col">Add activity</th>
                        <th scope="col">View</th>
                    </tr>
                </thead>
                <tbody>
                @foreach (Auth::user()->projects as $project)
                    <tr>
                        <td>{{ $project->name }}</td>
                        <td>
                            <a href="{{ route('admin.addActivity', ['id' => $project->id])}}">Add Activity</a>
                        </td>
                        <td>
                            <a href="{{ route('admin.showActivity', ['id' => $project->id])}}">View Activity</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <br>
        <a href="{{ route('logout') }}">Logout</a>
    </div>
</body>
</html>
