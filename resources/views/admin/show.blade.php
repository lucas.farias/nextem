<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap Simple Login Form</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .login-form {
            width: 340px;
            margin: 50px auto;
        }
        .login-form form {
            margin-bottom: 15px;
            background: #f7f7f7;
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            padding: 30px;
        }
        .login-form h2 {
            margin: 0 0 15px;
        }
        .form-control, .btn {
            min-height: 38px;
            border-radius: 2px;
        }
        .btn {
            font-size: 15px;
            font-weight: bold;
        }
    </style>
    <title>Page</title>
</head>
<body>
    <div class="flex-center position-ref full-height">
        <h2>
            Show Activity
        </h2>
        <div class="card-group">
            <table class="table">
                <tr>
                    <th>Activity</th>
                    <th>Description</th>
                    <th>Responsible</th>
                    <th>Status</th>
                    <th>Deadline</th>
                </tr>
                @foreach ($activitys as $activity)
                    <tr>
                        <td>{{ $activity->name }}</td>
                        <td>{{ $activity->description }}</td>
                        <td>{{ $activity->users->name }}</td>
                        <td>{{ $activity->status->name }}</td>
                        <td>{{ $activity->date_end }}</td>
                    </tr>
                @endforeach
            </table>

        </div>
        <br>
        <a href="{{ route('logout') }}">Logout</a>
    </div>
</body>
</html>
