<?php

namespace App\Http\Controllers\API;
use Config;
use JWTAuth;
use App\User;
use Exception;
use App\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt(['email' => $request->email, 'password' => $request->password, 'is_admin' => 1]))
            {
                return response()->json(['error' => 'User not exists!'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'Not create token'], 500);
        }

        return response()->json(['token' => $token, 'user' => $request->email]);
    }

    public function getAuthenticatedUser()
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        return response()->json(['user' => $user]);
    }

    public function getProjects()
    {
        $projects = Project::all();
        return response()->json(['projects' => $projects]);
    }

    public function sendRequestToApi(Request $request)
    {
        try {
            return response()->json([$request->all()]);
        } catch (Exception $e) {
            return response()->json(['error' => 'Time out!']);
        }
    }
}
