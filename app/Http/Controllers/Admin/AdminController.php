<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Status;
use App\Project;
use Illuminate\Http\Request;
use App\Services\AdminService;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{

    protected $service;

    public function __construct(AdminService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $auth = auth()->user();
        return view('admin.index');
    }

    public function addActivity($id)
    {
        $users = User::all();
        $status = Status::all();
        return view('admin.activity', ['users' => $users, 'status' => $status,'id' => $id]);
    }

    public function store(Request $request)
    {
        $response = $this->service->store($request->all());
        if($response['success'])
        {
            return redirect()->route('admin.index')->with('success', $response['message']);
        }
        else
        {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function storeActivity(Request $request, $id)
    {
        $response = $this->service->addActivity($request->all(), $id);
        if($response['success'])
        {
            return redirect()->route('admin.index')->with('success', $response['message']);
        }
        else
        {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function showActivity($id)
    {
        $project = $this->service->findProject($id);
        // return $project;
        return view('admin.show', ['activitys' => $project]);
    }

}
