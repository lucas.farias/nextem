<?php
namespace App\Services;

use Exception;
use App\Project;
use App\Activity;
use App\Http\Controllers\Admin\AdminController;

class AdminService
{

    public function store($data)
    {
        $user = auth()->user();
        try {
            $response = $user->projects()->create($data);
            return [
                'success'  => true,
                'message' => "Project create with success!",
                'data'     => $response,
            ];

        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => "Failed to create!",
            ];
        }
    }

    public function addActivity($data, $id)
    {
        try {
            $project = Project::find($id);
            $response = $project->activitys()->create($data);
            return [
                'success'  => true,
                'message' => "Activity create with success!",
                'data'     => $response,
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => "Failed to create!",
            ];
        }
    }

    public function findProject($id)
    {
        $project = Project::where('id', $id)->with('activitys.users')->first();
        return $project->activitys;
    }
}
