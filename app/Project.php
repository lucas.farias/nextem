<?php

namespace App;

use App\Activity;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'name', 'user_id',
    ];

    public function activitys()
    {
        return $this->hasMany(Activity::class);
    }
}
