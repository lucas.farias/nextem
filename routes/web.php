<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/login', 'Site\LoginController@index')->middleware('guest')->name('login');
Route::post('/login', 'Site\LoginController@postLogin')->name('auth.login');
Route::get('/logout', 'Site\LoginController@logout')->name('logout');


Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'admin'], function(){
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::post('/store', 'AdminController@store')->name('admin.store');
    Route::get('/storeActivity/{id}', 'AdminController@addActivity')->name('admin.addActivity');
    Route::post('/storeActivity/{id}', 'AdminController@storeActivity')->name('admin.storeActivity');

    Route::get('/showActivity/{id}', 'AdminController@showActivity')->name('admin.showActivity');
});

Route::get('/', function () {
    return view('welcome');
});
