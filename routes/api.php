<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



Route::post('auth/login', 'API\UserController@authenticate');

Route::group(['middleware' => ['throttle:60,1', 'jwt.verify']], function() {
    Route::get('user', 'API\UserController@getAuthenticatedUser');
    Route::get('projects', 'API\UserController@getProjects');
    Route::post('sendRequestToApi', 'API\UserController@sendRequestToApi');
});
