## Instalar dependências do projeto

composer install

## Configurar variáveis de ambiente

cp .env.example .env <br>
php artisan key:generate

## Criar migrations (Tabelas e Seeders)
php artisan migrate --seed

## Usuário de teste

email: teste@nextem.com.br<br>
senha: 1234

## Funcionalidades e Rotas
Rotas: (/login, /logout) as demais rotas são ações que se fazem na aplicação <br>

1 - Criar projetos<br>
2 - Adicionar atividade no projeto com os campos que foram requeridos (Add Activity)<br>
3 - Lista todas as atividades relacionadas ao projeto e o usuário que foi desifinado (View Activity) 

## API e Rotas
Rotas: (auth/login) - para conseguir o token<br>
Rotas: (user) - realiza o get do usuário de todas suas informações<br>
Rotas: (projects) - retorna todas os projetos cadastrados<br>
Rotas: (sendRequestToApi) - enviar requisição post
